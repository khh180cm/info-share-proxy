# Proxy for info-share project

This is for nginx proxy for info-share app


## Usage

### Environment Variables

 * `LISTEN_PORT` - Port to listen on (default: `8000`)
 * `APP_HOST` - Hostname of the app to forward request to (default: `app`) 
 * `PORT` - Port of the app to forward request to (default: `9000`)

